<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'tutowap' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'tutoWPadm' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '12345678' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=w-%;A@`Cr? hZyO9o+/mf-S`EE]Uu mkz$8Vc6lLZVH218FQL/j,-o~VAjr_q!W' );
define( 'SECURE_AUTH_KEY',  'r`1hSb}}#)h-jRh[XMP4JLsjU^9HIIxdZ=jsIKC4TfoOv==zG4UD=F=$k^yvF{rg' );
define( 'LOGGED_IN_KEY',    '/BZdS1VM/OCDN5nDY(%|r^(zPkx28~_oLQis[nO>[y.rl>Kz^g|k?Ga(]+MC]$8;' );
define( 'NONCE_KEY',        '@2`~%Z3X|c-ExIQO,({*R,=QKiavxq#G_sBZ/;d_A(1]+I_NKO??f_gCYk<o5 Y$' );
define( 'AUTH_SALT',        '$4)?YT6GzoKD3F(-T9M1t78^KCmuF&jP7aLi%]#<=F>c-qURkO r$_<~.X`jR6W[' );
define( 'SECURE_AUTH_SALT', 'j^tGcQfR2Kjo;537xal_u$XgEB_CaU?uwnU5-%/B!oV!< 7U]v]fhmCN^U^f<gVm' );
define( 'LOGGED_IN_SALT',   '97gaF5lv=l9-yIAoKP0K8f7E@gT*s_T2T<,@jIoF/PV.$8nLq[+-<?&Yw;RpTmp$' );
define( 'NONCE_SALT',       '>:&MGt0`)CwUFtnYMN$ m&YKs~i&8^8kJD=#G,S)2BV3B88/Y,-39oLctwH[gX0<' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'toto_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
