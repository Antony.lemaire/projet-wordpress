<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
	<img class="imagePrincipal" src="<?php echo get_template_directory_uri(); ?>/imageEntete.jpg" />
			<img class="" src="<?php echo get_template_directory_uri(); ?>/giphy.gif" />
			<div class="error-404 not-found">
				<header class="page-header">
					<img  src="<?php echo get_template_directory_uri(); ?>https:/media.giphy.com/media/A9EcBzd6t8DZe/giphy.gif" />
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentynineteen' ); ?></h1>
			<?php	get_template_part( 'template-parts/content/content' );	?>
				</header><!-- .page-header -->
	</div>
		<div class="error-404 not-found">
			

				<!-- Afficher les derniers articles du blog -->
        <h1>Découvrez nos derniers articles :</h1>
           <ul>
             <?php
               $my_query = new WP_Query('showposts=5');
               while ($my_query->have_posts()) : $my_query->the_post();
             ?>
             <li>
				 <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
			 </li>
             <?php
                endwhile;
             ?>
           </ul>
	</div>
				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentynineteen' ); ?></p>
					
					<?php get_search_form(); ?>
				</div><!-- .page-content -->
			</div><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
